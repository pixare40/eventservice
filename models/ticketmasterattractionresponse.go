package models

type TicketMasterAttractionResponse struct{
	Embedded []Attraction `json:"_embedded"`
}
