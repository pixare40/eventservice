package models

//Event struct
type Event struct {
	ID   string
	Name string
	Type string
	Locate string
	URL  string
	Sales string
	Date string
	PriceRange string
	Attractions []Attraction
	PleaseNote string
	Extensions string
	Source string
	Promoter string
	Images []Image
	Venue Venue
	Info string
}

type Attraction struct{
	ID string
	Name string
	Type string
	Images []Image
}

type Image struct{
	Ratio string
	URL string
	Height string
	Width string
	Fallback string
}

type Artist struct{
	Name string `json:"name"`
	RealName string `json:"realname"`
	Cname string `json:"cname"`
}

type Venue struct{
	ID string
	Name string
	Type string
	Locate string
	Location string
	URL string
	Timezone string
	Address string
	City string
	Country string
	State string
	PostalCode string
}
