package models

type TicketMasterEventResponse struct{
	Embedded []Event `json:"_embedded"`
}
