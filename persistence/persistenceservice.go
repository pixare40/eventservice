package persistence

import (
	"context"
	"eventservice/models"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

type DataRepositoryService interface{
	Initialise()
	StoreEventData(events []models.Event)
}

type MongoDataRepository struct {
	client *mongo.Client
	eventsCollection *mongo.Collection
}

func NewMongoDataRepository() *MongoDataRepository{

	mdr := MongoDataRepository{}
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://admin:admin@localhost:27017"))
	if err != nil{ return nil}

	mdr.client = client

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	err = client.Connect(ctx)
	if err != nil { return nil}

	collection := mdr.client.Database("trackfiend_events").Collection("events")
	mdr.eventsCollection = collection

	return &mdr
}

func (m *MongoDataRepository) StoreEventData(events []models.Event){
	for _, event := range events{
		_, err := m.eventsCollection.InsertOne(context.Background(), event)
		if err != nil{
			continue
		}
	}
}