package endpoints

import (
	"errors"
	"eventservice/models"
)

//Service interface
type Service interface {
	GetArtistEvents(artist string) (models.Event, error)
	HelloWorld() (string, error)
}

type EventService struct{}

func (EventService) GetArtistEvents(artist string) (models.Event, error) {
	if artist == "" {
		return models.Event{}, errors.New("invalid artist provided")
	}

	return models.Event{}, nil
}

func (EventService) HelloWorld() (string, error) {
	return "Hello World", nil
}
