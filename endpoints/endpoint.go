package endpoints

import (
	"context"
	"eventservice/models"
	"github.com/go-kit/kit/endpoint"
)

type GetArtistEventsRequest struct {
	Artist string `json:"artist"`
}

type GetArtistEventResponse struct {
	Event models.Event `json:"event"`
	Err   string       `json:"err"`
}

type HelloWorldRequest struct{}

type HelloWorldResponse struct {
	Greeting string `json:"greeting"`
	Err      string `json:"err"`
}

func MakeGetArtistEventEndpoint(s Service) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(GetArtistEventsRequest)
		events, err := s.GetArtistEvents(req.Artist)
		if err != nil {
			return GetArtistEventResponse{events, err.Error()}, nil
		}

		return GetArtistEventResponse{events, ""}, nil
	}
}

func MakeHelloWorldEndpoint(s Service) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		greeting, _ := s.HelloWorld()
		return HelloWorldResponse{greeting, ""}, nil
	}
}
