package main

import (
	"eventservice/coordinator"
	"eventservice/persistence"
	"eventservice/services"
	"eventservice/transport"
	"log"
	"net/http"
	"os"
)

func main() {
	mongoDataRepository := persistence.NewMongoDataRepository()
	eventAggregator := coordinator.NewEventAggregator()
	queueCoordinator := coordinator.NewQueueCoordinator(eventAggregator)

	ticketMasterFetchService := services.NewTicketMasterService(eventAggregator, mongoDataRepository)

	//Start event population service
	ticketMasterFetchService.StartService()

	//Report ready
	queueCoordinator.StartListening()

	t := transport.NewHTTPHandler()

	log.SetOutput(os.Stdout)

	log.Fatal(http.ListenAndServe(":15000", t))
}

