package services

import (
	"eventservice/coordinator"
	"eventservice/persistence"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

const queryEndpoint = "https://app.ticketmaster.com/discovery/v2/"
const eventResource = "events.json"
const attractionResource = "attractions.json"
const apiKey = "GxZNvYT2SqEcdQS50BMnfKW9YetRgaR7"

type FetchEventsService interface{
	QueryArtistEvents(artistName string)
}

type TicketMasterService struct{
	ea *coordinator.EventAggregator
	mr *persistence.MongoDataRepository
}

func NewTicketMasterService(ea *coordinator.EventAggregator, mr *persistence.MongoDataRepository) *TicketMasterService{
	return &TicketMasterService{
		ea : ea,
		mr : mr,
	}
}

func (s *TicketMasterService) StartService(){
	//Do some work
	s.QueryArtistEvents("Trippie Redd")
}

func (TicketMasterService) QueryArtistEvents(artistName string){
	eventResourceUri := getUri(eventResource, artistName)
	attractionResourceUri := getUri(attractionResource, artistName)

	attractionResponse, err := http.Get(attractionResourceUri)

	eventResponse, err := http.Get(eventResourceUri)

	if err != nil || eventResponse.StatusCode != http.StatusOK{
		log.Printf("Failed to fetch Events for artist : " + artistName)
		return
	}

	defer eventResponse.Body.Close()
	defer attractionResponse.Body.Close()
	if eventResponse != nil {
		fmt.Println("We actually got a result back for artist : " + artistName)
		body, _ := ioutil.ReadAll(eventResponse.Body)
		fmt.Println(string(body))
	}

	return
}

func getUri(resource, searchParam string) string{
	queryParam := ""
	if resource == eventResource{
		queryParam = "attractionId"
	}else{
		queryParam = "keyword"
	}

	return queryEndpoint + resource +
		queryParam + searchParam + "&apikey=" + apiKey
}

