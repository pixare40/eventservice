package transport

import (
	"context"
	"encoding/json"
	"errors"
	"eventservice/endpoints"
	httptransport "github.com/go-kit/kit/transport/http"
	"net/http"
)

func NewHTTPHandler() *http.ServeMux{
	service := endpoints.EventService{}

	m := http.NewServeMux()

	getArtistEventsHandler := httptransport.NewServer(
		endpoints.MakeGetArtistEventEndpoint(service),
		decodeGetArtistRequest,
		encodeResponse,
	)

	helloWorldHandler := httptransport.NewServer(
		endpoints.MakeHelloWorldEndpoint(service),
		decodeHelloWorldRequest,
		encodeHelloWorldResponse,
	)

	m.Handle("/getArtistEvents", getArtistEventsHandler)
	m.Handle("/helloWorld", helloWorldHandler)

	return m
}


func decodeGetArtistRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request endpoints.GetArtistEventsRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, errors.New("no request payload")
	}
	return request, nil
}

func encodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}

func decodeHelloWorldRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return r, nil
}

func encodeHelloWorldResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}