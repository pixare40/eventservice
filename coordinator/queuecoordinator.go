package coordinator

import (
	"encoding/json"
	"eventservice/models"
	"fmt"
	"github.com/streadway/amqp"
	"log"
)

const url = "amqp://rabbitmq:rabbitmq@localhost:5672"

type QueueCoordinator struct{
	statusQueue *amqp.Queue
	eventQueue *amqp.Queue
	ch         *amqp.Channel
	conn       *amqp.Connection
	ea *EventAggregator
}

func NewQueueCoordinator(ea *EventAggregator) *QueueCoordinator{
	conn, err := amqp.Dial(url)
	if err != nil {
		failOnError(err, "Failed to connect to RabbitMQ")
		return nil
	}

	ch, err := conn.Channel()
	if err != nil {
		failOnError(err, "Failed to open channel")
	}

	args := make(amqp.Table)
	args["x-dead-letter-exchange"] = ""
	args["x-dead-letter-routing-key"] = "event_queue_error"

	eventQueue, err := ch.QueueDeclare(
		"event_queue",
		true,
		false,
		false,
		false,
		nil)

	statusQueue, err := ch.QueueDeclare(
		"status_queue",
		true,
		false,
		false,
		false,
		nil)

	failOnError(err, "Failed to declare a queue")

	qc := QueueCoordinator{
		statusQueue: &statusQueue,
		eventQueue: &eventQueue,
		ch:         ch,
		conn:       conn,
	}

	msgs, err := ch.Consume(
		eventQueue.Name,
		"",
		true,
		false,
		false,
		false,
		nil)
	failOnError(err, "Failed to register consumer")

	go consumeLoop(msgs, onMessageReceived)

	//defer conn.Close()

	return &qc
}

func onMessageReceived(delivery amqp.Delivery) {
	var artistsArr []models.Artist
	err := json.Unmarshal(delivery.Body, &artistsArr)
	failOnError(err, "Failed to unmarshal json")
	fmt.Println(artistsArr)
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func (qc *QueueCoordinator) StartListening(){
	qc.ch.Publish(
		"",
		qc.statusQueue.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body: []byte("ready"),
		})
}

func consumeLoop(deliveries <-chan amqp.Delivery, handlerFunc func(d amqp.Delivery)) {
	for d := range deliveries {
		// Invoke the handlerFunc func we passed as parameter.
		handlerFunc(d)
	}
}

